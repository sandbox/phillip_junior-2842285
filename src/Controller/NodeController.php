<?php
/**
 * @file
 * Contains \Drupal\node_finder\Controller\nodeController.
 */

namespace Drupal\node_finder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;

class NodeController extends ControllerBase {
  public function content() {
    return array(
        '#type' => 'markup',
        '#markup' => $this->t('Please enter your node'),
		$form = \Drupal::formBuilder()->getForm('Drupal\node_finder\Form\NodeBlockForm'),
    );
  }
}