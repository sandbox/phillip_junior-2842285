<?php

namespace Drupal\node_finder\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\search\SearchPageRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


class NodeBlockForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['node_block_id'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Node ID'),
      '#description' => $this->t('Search for a node'),
      '#default_value' => isset($config['name']) ? $config['name'] : '',
    );
	$form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Go'),
      '#button_type' => 'primary',
    );
    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('node_block_id')) <= 0) {
      $form_state->setErrorByName('node_block_id', $this->t('The node id is too short'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	drupal_set_message($this->t('Your node id is @number', array('@number' => $form_state->getValue('node_block_id'))));
    //$route = 'user.page' . $form_state->get('node_block_id');
    //$form_state->setRedirect(
      //$route,
      //array(),
      //array('query' => $query)
    //);
	 return $this->redirect('entity.node.edit_form', ['node' => '2']);
	}

}
