<?php

namespace Drupal\node_finder\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Hello' Block
 *
 * @Block(
 *   id = "node_block",
 *   admin_label = @Translation("Node finder"),
 *   category = @Translation("Forms"),
 * )
 */
class NodeBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {
     
     return array(
      $form = \Drupal::formBuilder()->getForm('Drupal\node_finder\Form\NodeBlockForm'),
    );
  }
}